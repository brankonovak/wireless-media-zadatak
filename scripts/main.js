(function(){
	var $body = $('body'),
		$searchWrapper = $('.js-search-wrapper'),
		$nav = $('.js-nav-items-mobile');

	// showing nav and hiding search on mobile
	$(document).on('click', '.js-nav-icon', function () {
		$body.removeClass('css-js-search-input');
		$body.toggleClass('css-js-active-nav');
		$nav.stop().slideToggle(200);

		if ($searchWrapper.is(':visible')) {
			$searchWrapper.slideUp(200);
		}
	});

	// showing search 
	$(document).on('click', '.js-search', function () {
		$body.removeClass('css-js-active-nav');
		$nav.slideUp(200)

		if ($body.hasClass('css-js-search-input')) {
			$body.removeClass('css-js-search-input');
			$searchWrapper.stop().slideUp(200);
		} else {
			$searchWrapper.stop().slideDown(200);
			$body.addClass('css-js-search-input');
		}
	});

	// hiding nav on resize
	$(window).on('resize', function () {
		if (matchMedia("(min-width: 480px)").matches) {
			$body.removeClass('css-js-active-nav');
			$nav.slideUp(200);
		}
	});

	// scroll body to 0px on click
	$('.js-back-to-top').on('click', function () {
		$('body, html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
}());